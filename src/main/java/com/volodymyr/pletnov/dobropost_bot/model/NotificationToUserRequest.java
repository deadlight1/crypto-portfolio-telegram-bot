package com.volodymyr.pletnov.dobropost_bot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

@Data
@Builder
@JsonIgnoreProperties
@AllArgsConstructor
@NoArgsConstructor
public class NotificationToUserRequest {
	private String coinName;
	private String coinSymbol;
	@Digits(integer = 12, fraction = 20)
	private BigDecimal profitInPercent;
	@Digits(integer = 12, fraction = 20)
	private BigDecimal profit;
	@Digits(integer = 12, fraction = 20)
	private BigDecimal price;
	@Digits(integer = 12, fraction = 20)
	private BigDecimal percentChange1h;
	@Digits(integer = 12, fraction = 20)
	private BigDecimal percentChange24h;

	public String toMessage() {
		StringBuilder stringBuilder = new StringBuilder();
		if (coinName != null) {
			stringBuilder.append("Coin name: ").append(coinName).append("\n");
		}
		if (coinSymbol != null) {
			stringBuilder.append("Symbol: ").append(coinSymbol).append("\n");
		}
		if (profitInPercent != null) {
			stringBuilder.append("Profit In Percent: ").append(profitInPercent).append("%\n");
		}
		if (profit != null) {
			stringBuilder.append("Profit: ").append(profit).append("$\n");
		}
		if (price != null) {
			stringBuilder.append("Price: ").append(price).append("$\n");
		}
		if (percentChange1h != null) {
			stringBuilder.append("Percent Change 1h: ").append(percentChange1h).append("%\n");
		}
		if (percentChange24h != null) {
			stringBuilder.append("Percent Change 24h: ").append(percentChange24h).append("%\n");
		}
		return stringBuilder.toString();
	}
}
